## CI/CD-pipeline example#1

Простейший пример CI/CD-pipeline демонстрирующий процесс сборки артефакта, тестирования и деплоя.
WEB-сайт написан на Python (Flask) храняший часть данных в MySQL.
При коммите в ветку master срабатывает gitlab-runner устанвленный на моем сервере и пересобирает артефакт.

### How to install on your server

Клонируем репу:
```
git clone https://gitlab.com/dan.klassik/cicd-example1.git
cd cicd-example1/
```

Переопределяем переменные необходимые для соединения web-приложения с MySQL:
```
cp .env.example .env
```
Альтернативным вариантом является использоватние ключая --env-file=./.env.example при запуске docker-compose

Убеждаемся что конфигурация настроена верно:
```
docker-compose config
```
Запускаем:
```
docker-compose up -d
```
  
Web-сайт будет доступен на сервере по порту 5003 (при настройке 5000-5002 были заняты)


